#include <stdio.h>
#include <stdlib.h>
#include <string.h>

typedef struct node
{
	struct node *left;
	struct node *right;
	
	struct node *next;
	
	char letter;
	int amount;
	
} *node;

struct {
	char b0:1;
	char b1:1;
	char b2:1;
	char b3:1;
	char b4:1;
	char b5:1;
	char b6:1;
	char b7:1;
} byteToWrite;

node Head;

void printList();
void addNode(char letter, int amount);
void detachNode(node toDetach);
node extractMin();
void insertNode(node toInsert);
void huffman();
void goDeeper(node current, int index, char *code);
void printResult();
void countOccurrences(FILE *file, char *alphabet);
void printOccurrences(char *alphabet);
void buildList(char *alphabet);
void deeperCode(char **codes, node current, int index, char *code);
void buildCodes(char **codes);
void encodeFile(FILE *source, char *outputName, char **codes);


int main(int argc, char *argv[])
{
	if(argc == 3)
	{	
		FILE *file = fopen(argv[1], "r");
		if(file != NULL)
		{
			Head = NULL;
			char alphabet[128] = {0};
			char *codes[128];
			for(int i=0; i<128; i++)
				codes[i] = malloc(20 * sizeof(char));
			
			countOccurrences(file, alphabet);
			printOccurrences(alphabet);
			buildList(alphabet);
			huffman();
			printResult();
			rewind(file);
			buildCodes(codes);
			encodeFile(file, argv[2], codes);
			
			fclose(file);
		}
		else printf("\n Nie mozna otworzyc pliku \"%s\".\n Brak pliku lub uprawnien.\n\n", argv[1]);
	}
	else printf("\n Poprawne wywolanie:\n ./nazwa_programu nazwa_pliku_zrodlowego nazwa_pliku_docelowego\n\n");

  return 0;
}

void printList()
{
	node current = Head;
	
	printf("\n");
	while(current != NULL)
	{
		if(current->next != NULL)
			printf("%d->", current->amount);
		else
			printf("%d\n\n", current->amount);
		current = current->next;
	}
}

void addNode(char letter, int amount)
{
	node new = malloc(sizeof(struct node));
	
	new->letter = letter;
	new->amount = amount;

	new->left = NULL;
	new->right = NULL;
	
	if(Head == NULL)
	{
		new->next = NULL;
		Head = new;
	}
	else
	{
		new->next = Head;
		Head = new; //new Head
	}
}

void detachNode(node toDetach)
{
	if(toDetach != Head)
	{
		node current = Head;
		
		while(current->next != toDetach)
			current = current->next;
			
		current->next = toDetach->next;
	}
	else Head = Head->next;
}

node extractMin()
{
	node min = Head;
	node current = Head->next;
	
	while(current != NULL)
	{
		if(min->amount > current->amount)
			min = current;
		
		current = current->next;
	}
	detachNode(min);
	
  return min;
}

void insertNode(node toInsert)
{
	toInsert->next = Head;
	Head = toInsert;
}

void huffman()
{
	while(Head->next != NULL)
	{
		node x = extractMin();
		node y = extractMin();
		
		node toInsert = malloc(sizeof(struct node));
		
		toInsert->letter = '\0'; //no letter
		toInsert->right = y;
		toInsert->left = x;
		toInsert->amount = x->amount + y->amount;
		insertNode(toInsert);
	}
}

void goDeeper(node current, int index, char *code)
{
	if(current->letter == '\0')
	{
		index++;
		code[index] = '0'; code[index+1] = '\0';
		goDeeper(current->left, index, code);
		code[index] = '1'; code[index+1] = '\0';
		goDeeper(current->right, index, code);
	}
	else if(current->letter >= 32  &&  current->letter <=126)
			printf(" %c: %s\n", current->letter, code);
}

void printResult()
{
	char code[20] = {'\0'};
	node current = Head;
	int index = -1;
	
	printf("\n\n");
	printf("Litera: kod\n");
	goDeeper(current, index, code);
	printf("\n\n");
}

void countOccurrences(FILE *file, char *alphabet)
{
	char ch = getc(file);
	
	while(ch != EOF)
	{
		alphabet[(int)ch]++;
		ch = getc(file);	
	}
}

void printOccurrences(char *alphabet)
{
	printf("\n\nLitera: ilosc wystapien w tekscie\n");
	int i;
	for(i=32; i<=126; i++)
		if(alphabet[i] > 0)
			printf(" %c: %d\n", i, alphabet[i]);	
}

void buildList(char *alphabet)
{
	int i;
	for(i=0; i<128; i++)
		if(alphabet[i] > 0)
			addNode(i, alphabet[i]);
}

void deeperCode(char **codes, node current, int index, char *code)
{
	if(current->letter == '\0')
	{
		index++;
		code[index] = '0'; code[index+1] = '\0';
		deeperCode(codes, current->left, index, code);
		code[index] = '1'; code[index+1] = '\0';
		deeperCode(codes, current->right, index, code);
	}
	else strcpy(codes[(int)current->letter], code);
}

void buildCodes(char **codes)
{
	char code[20] = {'\0'};
	node current = Head;
	int index = -1;
	
	deeperCode(codes, current, index, code);
}

void encodeFile(FILE *source, char *outputName, char **codes)
{
	FILE *output = fopen(outputName, "w");
	
	int i = 0;
	char codeToWrite[20];
	char charToWrite;
	
	char ch = getc(source);
	
	while(ch != EOF)
	{	
		strcpy(codeToWrite, codes[(int)ch]);
		for(int j=0; j<strlen(codeToWrite); j++)
		{
			
			charToWrite = codeToWrite[j];
			if(charToWrite == '0') charToWrite = '\x00';
			else charToWrite = '\x01';

			switch(i)
			{
				case 0: byteToWrite.b0 = charToWrite; break;
				case 1: byteToWrite.b1 = charToWrite; break;
				case 2: byteToWrite.b2 = charToWrite; break;
				case 3: byteToWrite.b3 = charToWrite; break;
				case 4: byteToWrite.b4 = charToWrite; break;
				case 5: byteToWrite.b5 = charToWrite; break;
				case 6: byteToWrite.b6 = charToWrite; break;
				case 7: byteToWrite.b7 = charToWrite;
			}
			
			if(i == 7)
			{
				fwrite(&byteToWrite, 1, 1, output);
				i = -1;
				byteToWrite.b0 = byteToWrite.b1 = byteToWrite.b2 = byteToWrite.b3 = 
				byteToWrite.b4 = byteToWrite.b5 = byteToWrite.b6 = byteToWrite.b7 = '\x00';
			}
			
			i++;
		}
		ch = getc(source);
	}
	
	if(i != 8) fwrite(&byteToWrite, 1, 1, output);
	
	fclose(output);
}

/*void encodeFile(FILE *source, char *outputName, char **codes)
{
	FILE *output = fopen(outputName, "w");
	
	char ch = getc(source);
	
	while(ch != EOF)
	{
		fprintf(output, "%s", codes[(int)ch]);
		ch = getc(source);
	} 
	
	fclose(output);
}*/
